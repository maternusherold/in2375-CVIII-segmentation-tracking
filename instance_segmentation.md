# Computer Vision III - Instance \& Panoptic Segmentation 

__Segmentation: label every pixel with a class__

Semantic segmentation: segmentation, incl. background, but don't differentiate between pixels belonging to the same class

Instance segmentation: segmentation but without uncountable objects, e.g. background, grass etc. BUT differentiate between instances of a class, i.e. car 1, car 2, ...

Panoptic segmentation: instance segmentation but with a generic label for uncountable objects, e.g. sky, grass


## 1. Instance Segmentation

 - What are the different instance segmentation methods?
   - proposal-based: (1) proposals, e.g. detection, (2) assigning class
   - fully conv. network: (1) semantic segmentation (2) differ between instances 


 - Why do we use fully conv. networks?
   - to use any size of input 


#### Mask R-CNN

Combination of both, the proposal and fully conv. approach as proposal based methods yield better performance and fully conv. can be better used.


 - What is the structure of Mask R-CNN?
   - use Faster R-CNN with region proposal network etc.
   - add a third head: instance segmentation via. fully conv. net
   - third head works on detections and produces segmentation
   - loss for seg. head: BCE per pixel for all classes 

        <img src="./figures/mask_rcnn.png" width=500>


 - What is an equivariant map?
   - simple: functions for which the value stays unchanged due to symmetry transformations on the input
   - i.e. produces the same output on both scaled, translated etc. inputs 
   - but, it __varies equally__ with the distortion
   

 - What are the differences between detection and segmentation?
   - detection: find the obj. in the scene, if present; need an invariant representation
   - segmentation: needs equivariant map to detect on all scales, all location etc. 
   - important: while for semantic segmentation small objects have less importance, for instance seg. all objects count 


  - What is the difference between equivariant and invariant representations?
    - invariant: no variation; $f(x) = f(x-5)$
    - equivariant: small variations; $f(x) \propto f(\alpha x)$


 - Why do we have to change RoI Pooling?
   - fully connected layers and pooling are invariant
   - but need equivariant 


 - What is the Quantization Effect?
   - if the RoI in the input image is of size x,y then the size is a fraction of it in the feature map
   - due to RoI pooling where the grid is put ontop, pixel-wise segmentation is not precise


 - What is RoI Align?
   - equivariant version of RoI pooling
   - __avoiding quantization effect__
   - uses bilinear interpolation for the grid points which are sampled 4 times
   - improves masks as well 

      <img src="./figures/mask_rcnn_roi_align.png" width=500>


 - How can the Mask R-CNN be extended for joints?
   - let model predict k masks of 1 pixel
   - each keypoint gets a one-hot mask
   - for each instance, let the model predict on mask for the k keypoint types, e.g. left shoulder, right shoulder, ...


 - What is a shortcumming of the initial Mask R-CNN?
   - loss acted on box level but not on instance level
   - i.e. might have correct box and semantic but pixels might be wrong
   - solution: use IoU head between predicted and ground truth mask; could also use CRFs


 - Is the confidence for the Mask-RCNN or improved Mask-RCNN better?
   - confidence higher for Mask RCNN because:
   - as Mask-RCNN only looks at the bounding boxes and the class label stays the same
   - for the improved loss, the IoU score will be less, implying a smaller confidence 


#### YOLACT

similar to YOLO, i.e. a one-stage detector, and thus faster than the two-stage detector Mask R-CNN, but also less performant. While Joseph Redmon was not able to get YOLO to learn masks, the YOLACT learns mask coefficients instead.


 - How is YOLACT structured?
   1. Feature extraction backbone, e.g. ResNet-101
   2. Deeper layers are connected to feature pyramid; i.e. features computed at different scales 
   3. Protonet: FCN, generates $k$ mask prototypes 
   4. Mask coefficients: predict $k$ coefficients per anchor, i.e. one per prototype
   5. Mask assembly: linear comb. of prototyped masks and coefficients; lin. comb. is learned
   6. predict via. non-linearity 
   7. for masks: CE loss 

        <img src="./figures/yolact_overview.png" width=500>


 - How does YOLCAT perform?
   - very fast and good for large objects, sometimes even better than for two-stage
   - so, take YOLACT when predictions need to be fast and can be a little less accrurate than Mask- or MS-RCNN


## 2. Panoptic Segmentation

Similar as instance segmentation but including a generic label for uncountable objects, s.a. grass or sky. As there are already two kinds of methods doing this, i.e. segmentation and instance segmentation, they are joined for a panoptic segmentation model where a parameter-free panoptic-head joins the information of both to not have overlapping results. Popular architecture: UPSNet.


 - What are deformable convolutions?
   - special case of dilated convolutions
   - learns the __offset parameter__
   - picks the convolution values at different locations, conditioned on the input feature maps 
   - i.e. learns from where to get the information

        <img src="./figures/panoptic_seg_deformable_con.png" width=500>


 - What are the advantages of deformable convolutions?
   - conditioned on feature maps
   - can pick values at different location, not just rectangular
   - thus, being more precise esp. at boundaries with non-regular shapes
  

 - What is the panoptic head?
   - <b style="color:red">TODO ...</b>


 - How are _stuff_ and _things_ treated differently by the panoptic head?
   - _stuff_ is passed directly to the panoptic logit map
   - _things_ first need to be masked by the corresponding instances before being passed to the logit map


 - What is the reason for unknown section in panoptic results?
   - compare panoptic quality loss
   - if something is false predicted, then the PQ loss is influenced twice as FP and FN
   - with unknown, we only have False Negative influence 


## 3. Metrics for Panoptic Segmentation

 - What is panoptic quality?
   - consists of segmentation quality and recognition quality
   - segmentation quality: how close are predicted segments to ground truth: $\frac{\sum_{(p,g) \in \text{TP}} \text{IoU}(p,g)}{|\text{TP}|}$
   - recognition quality: if any instances are missing (FN) or too many (FP) - as in detection: $\frac{|\text{TP}|}{|\text{TP}| + 0.5 |\text{FP}| + 0.5 |\text{FN}|}$
   - segments are again matched via IoU


## 4. Voting

e.g. Hough voting or object detection as voting

<b style="color:red">skipped for now</b>


 - How does instance voting work?
   - predict for each pixel, if it belongs to instance mask, and if the relative location of the mask's centroid
     1. discretize region around each pixel
     2. pixels vote for centroid of the target or non, if stuff
     3. aggregate votes at pixels; cast to accumulator space using transposed convolutions
     4. detect objects as peaks in acc. space 
     5. backproject peaks into image and get instance mask, i.e. which pixels voted for that one
     6. category info. comes from semantic seg. head

        <img src="./figures/panoptic_seg_voting.png" width=500>


<b style="color:red">Voting Look up table?</b>




## Open Qs

 - What is MS-RCNN exactly?

