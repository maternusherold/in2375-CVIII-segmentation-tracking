# Computer Vision III, IN2375

_summary and questions for self-study of the content covered in IN2375, summer term 2021_

concerning the exam:
 - each big topic has a specific metric
 - each topic has a specific method 
 - the exam won't cover all the "old" stuff
 - finally, the exam is solely based on the slides and thus cannot go to far into depth
