# Computer Vision III - Semantic Segmentation 


 - What are the key features of $1 \times 1$ conv. that we use?
   - keeps spatial dimensions and scales input
   - change size of feature map
   - further non-linearity without changing size 
   - network in network as can be seen as a fully connected layer on the channels of a pixel


 - Why are layers deep in the net called high-level?
   - as they hold information aggregated across many feature maps 
   - as info is aggregated, it is on a higher level 


 - Why are low-level features so important?
   - used for fine tuning as they hold the pixel information 
   - refine the bounding box
   - high-level features give coarse bounding boxes


 - What are different types of upsampling?
   - Interpolation
   - fixed unpooling + convs. to refine 
   - Unpooling/transposed conv. network
   - while the first ones are fast, the third ones keeps a lot of detail by remembering where the max. values came from during pooling


 - Why does UNet use skip connections?
   - to transfer low-level information from encoder to decoder 
   - bottleneck is very high-level and might loose low-level feat. 


 - What are key-challenges in segmentation?
   - reduced feature resolution; going up is hard -> dilated (atrous) convolutions
   - objects at multiple scales, small ones tend to get lost -> pyramid pooling
   - poor localization of edges -> refinement with cond. random field (CRF)


## 1. Dilated Convolution 

 - Why can't we use convolutional layers of the same spatial size as the image s.t. we don't lose resolution?
   - too expensive 


 - What are dilated/atrous convolutions?
   - dense feature extraction when upsampling
   - let's the receptive field grow exponentially while the number of parameter grows linearly 
   - dilation parameter $k$: the $k$-th pixel is the next used


 - Why does it make senese to use dilated convs. with high res. inputs?
   - many pixels have similar information
   - so local information is very much alike 
   - skipping some pixels to get information is useful to jump into different regions 
   

 - What are some cons of dilated convolutions?
   - skipping information on lower res. images 
   - cannot be used for e.g. in OCR as line width is importnat 


 - How are deformable convolutions connected to dilated convolutions?
   - deformable convolutions can learn their offset 
   - dilated convs cannot but also have an offset
   - i.e. dilated convs are a special kind of dilated convs. (according to lecturer)


 - How do you compute the receptive field of a dialted conv.?
   - given dilation parameter $k$; kernel size $f$ and previous receptive field size $r_{in}$
   - $r_{out} = r_{in} + (f - 1) \cdot k$
   - in the example, we have:
     - $3 = 1 + (3 - 1) \cdot 1$
     - $7 = 3 + (3 - 1) \cdot 2$
     - $15 = 7 + (3 - 1) \cdot 4$
   - note how the dilation factor was increased linearly by a factor of 2
   
      <img src="./figures/dilated_conv_receptive_field.png" width=500>
   

## 2. Conditional Random Fields

used in DeepLab but <b style="color:red">skipped</b>. Also, they hinder the network to be trained end-to-end as the fully conv. network and the CRF are trained separately. Which makes training slow and suboptimal. Solution would be to formulate CRFs as RNNs, which makes the algorithm end-to-end trainable.


 - What is the main use of CRF?
   - properly localize the masks, i.e. refining the contours
   - is conditioned on the RGB input and thus has information about the original resolution

 - How can CRFs be replaced?
   - Attention


 - What is soft attention?
   - all attention masks $\alpha_i$ add to 1


 - What is context?
   - aggregates the different attention values for different positions $k$ for the next prediction $t+1$
   - $c_{t+1} = \sum_k \alpha_{k,t+1} a_k$ where $a_k$ an embedding of the input at position $k$


<b style="color: red">Seq2Seq</b>


 - What are benefits of Attention Networks in segmentation?
   - they can learn to put attention on objects at different scales


... in the end DeepLab v3 does not use any of CFR, RNN or Attention


## 3. DeepLab v3

combines atrous convolutions, spatial pyramid pooling and a encoder-decoder style.


<b style="color: red"> TOOD... </b>


 - What are depth-wise separable convolutions?
   - filters are applied on single features, i.e. single features per channel
   - pro: uses less computations 


        <img src="./figures/compute_diff_convs.png" width=500>


 - How does DeepLab v3 save computations?
   - uses depth-wise separable convolutions
   - e.g. $k=5 \times 5$ on a map of $9 \times 9 \times 7$ needs: $5 \cdot 5$ multiplications per kernel in $5 \cdot 5$ locations of the map, on $7$ channels, i.e. $5^4 \cdot 7$ multiplications

## 4. Important Metrics

 - IoU
 - mean IoU = MIoU: compute IoU for each class and then the mean
 - ratio of pixels classified correctly
