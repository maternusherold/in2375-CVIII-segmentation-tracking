# Computer Vision III - Overview


## 0. Covered Concepts

 - Covered Architectures
   - RCNN, Fast RCNN and Faster RCNN (two-stage detectors)
   - YOLO, SSD, RetinaNet (one-stage detectors)
   - Siamese networks - Re-appearing objects 
   - Message passing networks 
   - Network flow for tracking (non-neural network in this case)
   - GANs for trajectory prediction
   - Mask-RCNN, UPSNet for panoptic segmentation (including background)
   - Deformable convolutions
   - 3D algorithms 


 - Example Methods/Architectures per task
   - Semantic segmentation: e.g. FCN or UNet
   - Instance segmentation: e.g. Mask R-CNN
   - Panoptic segmentation: e.g. UPSNet


## 1. Intro

 - what is Computer Vision and why is it so popular for robotics?
   - generating information about a scene purely on recorded data using several sensors such as cameras and lidar
   - gives eyes to a computer with the goal of: understanding every pixel of an image (semantic or instance-based segmentation) or video (multiple object tracking)
   - ability to give a robot some perception about its surrounding without any prior observations, i.e. ad hoc; needed for e.g. in autonomous driving 


 - difference between classification, classification & localization, object detection and instance segmentation
   - class.: keyword, what's in the scene
   - class. + local.: detect on object in the scene and draw a bounding box around it 
   - object detection: detect all objects in the scene and draw bounding boxes around all of them 
   - segmentation: detect the exact pixels that belong to a certain object class
   - class * = __single__ object
   - obj. detection & seg. = __multiple__ objects 


 - how to we grade the levels of "image understanding"?
   - detection: coarse level, just bounding boxes, where the objects are, i.e. that there are some in the scene 
   - segmentation: precise level, assign same pixel values to an object class
   - semantic seg.: assign the same pixel value to objects of the same class, i.e. all cars in red, but does not differ between single instances s.t. several houses could be colored in red (semantic). In the case of instance based segmentation, all instances of a class are segmented individually, i.e. each house in street is outlined and also each house is now a different "prediction" and has different colors. This introduces further difficulties like occlusion.


 - what's the difference between segmentation, instance-based segmentation and panoptic segmentation?
   - segmentation: each object class gets the same lable and we only care about that there is such a class, i.e. there is a car/cars
   - instance: each instance is a results, i.e. each car, person etc. is labeled separately , i.e. there is person 1, car 1 & 2, ... 
   - panoptic: combination of both above where also a lable for uncountable objects like grass or sky is included, i.e. there is car 1 & 2 and also grass on which person 1 is standing


    <img alt="Panoptic vs. other segmentation" width="500" src="./figures/panoptic_segmentation_vs_others.png">


 - (simple) advantages and disadvantages of understanding a video
   - instead of a single image, one now has a stream and can use: (1) redundancy via several frames, (2) assumption of smooth changes in the scene (no jumps)
   - BUT (1) processing 30FPS is challenging; often even a higher stream rate and (2) videos include interaction of objects, occlusions and movements


 - what are our goals when trying to understand a video?
   - where is every object going -> direction and maybe prediction
   - how are objects interacting -> persons paths are crossing = deviation of both persons trajectories 
   - get consistent results in temporal dimension -> car is still car at t+1 
   - all while doing segmentation (detect different instances per-pixel) and semantics (know what that object is, e.g. a person) !

