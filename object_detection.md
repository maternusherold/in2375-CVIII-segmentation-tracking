# Computer Vision III - Object Detection


General task in object detection: 
 - detect all object(s) in scene
 - bounding box `(x,y,w,h)`
 - class

    <img src="./figures/object_detection.png" width="200">


## 1. Pre-DeepLearning Approaches

Pre-DeepLearning approach - _template matching_
 - use a template of the object and slide over the input image
 - compute correlation score of template and input 
 - problems: needed a very similar setup in the mask and input, i.e. no occlusion, same pose etc. __and__ instances instead of object classes werde detected, compare different kinds of chairs -> (1) occlusions, (2) shape/scale changes and (3) pose changes 


Pre-DeepLearning approach - _feature extraction_
 - use boosting, i.e. several week/simple classifiers to get many small decisions and combine such into a global decision
 - feature extraction: e.g. Viola-Jones detectors (masks) with Haar features or History of Gradients (HOG)
 - Viola Jones
   - select set of best feature extractors using AdaBoost -> final class. is linear combination of such
   - superfast approach for known setting but fails to generalise to new ones, e.g. head turned 
 - HOG
   - use avg. gradient of training set per class to provide _shape information_ -> shape descriptor
   - compute as histogram of gradient magnitudes 
   - train SVM classifier on a set of images containing that object and images not containing it 
   - further advances via. deformable parts where HOG features are computed per, e.g., body part 


Downside
 - no __class-agnostic__ measure describing the object class
 - instead, only instance representations were defined 
 - idea: generate a set of object proposals/regions of interest which might contain the object, measured on such a class-agnostic score
 - then: select the best of such proposals/ROIs


## 2. Selection Methods

Proposal Generation Methods
 - selective search
    1. compute initial sub-segmentation, very fine 
    2. combine small regions iteratively to larger ones using similarity scores (e.g. color, texture, size or fill similarity)
    3. use large regions as proposals and compute solution candidates 
      
      <img src="./figures/selective_search_for_obj_detection.png" width="500">

 - edge boxed: .... 


Selecting best proposal/ROI
 - region overlap, i.e. Jaccard index: $\text{IoU} = \frac{|A \cap B|}{A \cup B|}$
   - normalization needed to account for region sizes 
 - non-maximum suppression (NMS)
    1. for each proposal, compute the overlap with the other proposals; if the overlap is large enough, then we compare their scores for that object 
    2. compare the current box to all boxes with similar overlap $f_{\text{same}}(b_i, b_j) \geq \lambda_{\text{nms}}$
    3. if the other box has a better score, computed by a model on that proposal - e.g. CNN -, then discard the current one; otherwise keep it as candidate solution
   
      <img src="./figures/non_max_sup.png" width="500">


What's the problem the NMS hyperparameter $\lambda_{\text{NMS}}$?
 - too high: bounding boxes detecting the same obj. are all treated differently, i.e. __False Positives__ as boxes describing the same object, but worse than another box, are still taken into account 
 - too low: bounding boxes of even different objects (often close) are treated as describing the same, i.e. __False Negatives__ as boxes describing a new object are discarded  
 - in the below image, if the threshold is set too low, then the horses in the back will be discarded, as the first horse has a higher confidence score, i.e. will be selected, and the other two will be said there are non, i.e. false negatives.
    
    <img src="./figures/nms_problem.jpeg" width="500">

    <img src="./figures/nms_precision_recall.png" width=500>


What are two types of popular detector classes incl. popular references?
 - one-stage detectors
   - fast, end-to-end, less accurate
   - YOLO, SSD, RetinaNet, CenterNet, CornerNet, ExtremeNet
      <img src="figures/one_stage_detectors.png" width="500">
 - two-stage detectors
   - slower, intermediate steps (e.g. (1) feature extraction -> (2) obj. proposal extraction), very good performance 
   - R-CNN, Fast R-CNN, Faster R-CNN, SPP-Net, R-FCN, FPN
      <img src="figures/two_stage_detectors.png" width="500">


How are the localization and classification head trained/used?
 - usually, train the classification head, fix weights, then train regression head
 - sometimes, iteratively freeze the regression head and interchangeably train them in a loop
 - use both at test time in parallel 


Why are prediction candidates produces at different scales?
 - such that also smaller features are learned and not shadowed by big objects/features 


Why are we using region proposals?
 - trying all possible regions and different scales are expensive 
 - idea: select interesting regions which are then evaluated on -> (1) compute region proposal (2) classify per region proposal 


## 3. Two-Stage Detectors

#### Overfeat
  
  - How is the Overfeat model structured?
    - <p style="color: red">are the inputs to overfeat pre-segmented?</p> Taking possible positions at different scales?
    - sliding window - incorporated into CNN nature; yields a $5 \times 5 \times 1024$ output
    - box regression & classification - regression on fully connected layers 
    - workflow:
      - produce several window positions incl. scores per window
      - for all boxes with the same predicted object __and__ significant overlap compute box refinement into one
      - do a final prediction on the refined box; `out`: one box per class and scores for all classes, i.e. $1000 \times 4$ and $1000 \times 1$
    - __BUT__: only one object at a time and only on images of fixed size


#### R-CNN

 - How is the basic R-CNN structured?
   - compute ~2k region proposals using selective search
   - warp region proposals into std. dimension of $227 \times 227$
   - compute feature maps on warped inputs using CNN
   - apply regression and classification (SVM) head on feature maps
   
      <img src="./figures/rcnn_workflow.png" width=500>


 - How is the RCNN trained?
   - use pre-trained feature extraction, e.g. VGG-16
   - finetune the feature extractor of number of classes which will be distinguished -> as we have Softmax loss
   - train one SVM for classification __per class__ using hinge loss
   - train bounding box regressor using $\mathcal{L}_2$ loss

 - What are the __PROS__ of the RCNN?
   - well-known and tested pipeline from region proposal, feature extraction and SVM classification -> compare HOG where we now use a CNN
   - CNN compresses each proposal into 4096 dim. vector -> much less than HOG
   - can use any size of image as we only need proposals 
   - leverages transfer learning from ImageNet competition 


 - What are the __CONS__ of the RCNN?
   - separate components 
   - __slow!__ as $47$s/image with a VGG16 backbone 
   - slow and complex training as non-connected parts, also we deal with the same pixels multiple times due to overlapping regions  
   - learning cannot be fully leveraged as components are disconnected, i.e. cannot influence each other on better representations 
   - have to train one SVM per class, i.e. expensive for many classes 


 - How does SPP-Net solve slow inference of RCNN?
   - uses one single CNN as feature extractor on the whole image; region proposals are still computed by e.g. selective search 
   - regions are then extracted in the feature map of the CNN
   - shared computation in the big CNN reduced test time
   - but still no end-to-end learning, slow training and complex training schema and how to take from the features in different sizes? 


#### Fast R-CNN

The Fast R-CNN family which is end-to-end trainable, __expect__ for the region proposals.

 - How is the Fast R-CNN setup?
   - region proposals via. selective search
   - one CNN on the whole input image
   - region proposals from feature map using such from first step
   - RoI pooling to get into same shape
   - fully connected layers 
   - (linear) regression and classification heads, i.e. no SVMs!

      <img src="./figures/fast_rcnn.png" width=500>


 - How does RoI pooling work?
   - the region of interest/region proposal is embedded into the feature map after the CNN
   - the size of desired areas for the fully connected layers are put on top of of each proposal as a grid, e.g. $H \times W$
   - then, assuming the proposal is larger, we have several values in each cell of the $H \times W$ grid
   - apply max-pooling to each of the cells and use that value 
   - backpropagation works the same way as for other pooling operations 
      
      <img src="./figures/roi_pooling.png" width=500>


 - What is the bottleneck of the Fast R-CNN?
   - proposal generation
   - vanilla test time comparison: speedup of 146x comp. to R-CNN
   - but speedup shadowed by proposal generation as total speedup only 25x


#### Faster R-CNN

 - What solves the last bottleneck?
   - integrate proposal generation into pipeline
   - proposal generation now via _Region Proposal Network_
 
 
 - How is the Faster R-CNN structured?
   - (pre-trained) CNN on complete input
   - Region Proposal Network on feature map of CNN while feature map is __also__ passed directly to ROI pooling as the regions are taken from that map; just which regions need to be selected
   - on top of proposals in feature map, everything the same

      <img src="./figures/faster_rcnn_structure.png" width=500>


 - How does the Region Proposal Network work?
   - __retains spatial dimensions__ of feature map
   - per feature value, compute $3 \times 3$ convolution to generate $256$ encoding using $k$ anchor boxes
   - per anchor box, predict score for object \& no object; predict bounding boxes

      <img src="./figures/faster_rcnn_region_proposal_net.png" width=500>


 - What are important hyperparameters in Region Proposal Network?
   - number of proposals to generate = fixed number 
   - where are the proposals placed? densely, i.e. at each pixel


 - How are RPNs trained
   - compute anchors with a high overlap, e.g. with an IoU of $>.7$ with the ground truth
   - construct set of 256 anchors from a mini-batch where the set is equally distributed into object and non-object from first step
   - for classification compute Binary Cross Entropy loss
   - for regression compute smoothed $\mathcal{L}_1$ loss - only use boxes with an object 
   - finally, train RPN separate from rest, then jointly 
  

 - Which losses does the Faster R-CNN have?
   - RPN classification (obj. vs. non-obj)
   - RPN regression (anchor to refined proposal)
   - Faster R-CNN classification loss
   - Faster R-CNN regression loss (proposal to candidate solution)


 - What are the performance gains of Faster R-CNN?
   - 10x faster than Fast R-CNN
   - end-to-end training as feature extraction and proposal generation are included 
   - more accurate training capabilities are fully leveraged on all steps
   - RPN is fully convolutional
   - overall similar mAP as Fast R-CNN 


## 4. One-Stage Detectors

#### General

 - Which step is skipped compared to the two-stage detectors?
   - extraction of object proposals
   - the regression and classification heads directly work on the feature extraction 


 - One-stage detectors are way faster than two-stage ones 


 - The usual structure is
   - image
   - CNN
   - detection
   - i.e. going form input directly to the output 


#### YOLO

 - What are the advantages and disadvantages of YOLO vs. R-CNN family?
   - R-CNN way better performance 
   - YOLO way faster and all are end-to-end trainable 
   - YOLO are usually the go-to algorithm in real-time cases s.a. autonomous driving 
   - YOLO does not undergo region proposal step s.a. R-CNN, but only predicts on limited number of boxes by splitting image into grid


 - How does YOLO save compute time?
   - it limits its operations on a few areas in the image
   - organizes image into $S \times S$ grid where the CNN acts only on the grid cells
   - per grid cell, predict $n$ bounding boxes using predefined anchor boxes as well as confidence scores per box and class probabilities covering all boxes
   - predictions are thus encoded as $S \times S \times (n * 5 + C)$ where $C$ the number of classes $n$ the number of bounding boxes and the $5$ refers to four bounding box coordinates and a confidence score 


 - What does the confidence score in the YOLO net refer to?
   - how confident the model is, that it contains an object 
   - how accurate the box is that it predicts 


 - Why did the authors of YOLO family switch to Sigmoid for class scored from Softmax in v1 and v2?
   - Softmax does not allow for multilabeled data
   - multilabled data is not uncommon for hierarchical data sets 


 - How does YOLOv2 differ and how big is the output?
   - uses anchor boxes
   - $W \times H \times (C + 4 + 1) \cdot n$ where $n$ the number of anchors, i.e. per anchor we get class, bounding box and confidence 


#### SSD

 - Why do we have "skip-connections" in the SSD?
   - the connections pass directly from one of the CNN's feature map to the detection layer 
   - to be able to classify at different scales
   - idea: small objects might get lost when scaling further down into the network
   - thus: detect smaller objects in the beginning and larger ones in the end


 - What are the __pros__ and __cons__ of YOLO/SSD?
   - very fast, end-to-end trainable and fully convolutional, i.e. can work with any image size
   - SSD can work with more objects than YOLO due to extra classifiers, e.g. detecting smaller objects
   - performance not as good as two-stage
   - difficulty with small objects 


 - What is a general problem of one-stage detectors, esp. YOLO and SSD?
   - doing semantic classification and (non-)object classification at the same time
   - also all location (or even $n$ anchors per location) need to be analysed producing many easy classifications
   - solution focal loss -> focussing on harder samples to classify


#### RetinaNet

 - What is the change in RetinaNet compared to YOLO or SSD?
   - due to too many easy examples there is a loss imbalance, i.e. getting all the easy ones right outweighs learning on hard few ones
   - e.g. 100 hard ones wrong with $100 * 2.5 \ll 100000 * 0.1$ getting many easy one right
   - changed the loss function: _focal loss_ to reduce impact of simple examples 


 - How is the focal loss defined and how can it be tweaked?
   - $\text{FL}(p_t) = -(1-p_t)^{\gamma} \log (p_t)$ vs. $\text{CE}(p_t) = - \log (p_t)$
   - $\gamma = 0 \implies \text{CE}$ 
   - $\gamma \to \mathbb{N}_+$ down-weights simple examples 
   
      <img src="./figures/focal_loss_plot.png" width=500>


 - How does RetinaNet work?
   - uses ResNet as backbone for feature extraction
   - multi-scale predictions (compare SSD)
   - 9 anchors per level, each with regression and classification target 
   - focal loss 
  

### Two-stage vs. One-stage

Two-stage detectors
 - classification only on a preselected set of foreground regions, ~1-2k; i.e. filtering out background samples 
 - class balance between fore- and background objects is controllable 
 - concentration on analyzing proposals with rich information content 


One-stage detectors
 - need to analyze densely, ~100k locations - but that's still faster than generating proposals 
 - proposals include negatives, i.e. no signal
 - only a few positive examples for learning 


## 5. One-Stage Point-Based Detectors

Scale and aspect ratio of the anchor boxes needed to be picked by hand and are thus a hyperparameter. The goal is to "learn" such.


#### CornerNet

idea: define box via top-left and bottom-right corner 


 - What are the steps for CornerNet?
   - compute heatmap on the image using a CNN
   - for each corner proposal, compute probability of top-left or bottom-right corner 
   - per corner compute an embedding
   - match similar embeddings which are then top- and lower pairs forming a box
   - classify on boxes 


 - What are problems with CornerNet?
   - focusses on the corners rather than the center of the image
   - many incorrect boxes leading to false positives, i.e. especially small boxes w/o content 
   - hypothesis: hard to infer class when box is focussed on boundary -> focus on center 


 - How was CornerNet improved?
   - use corners as proposals 
   - use center to verify the class of object and filter out outliers, i.e. small boxes with no content 


#### ExtremeNet

hypothesis: predicting corners of an object is not ideal as the corners might not lie on the object and thus are hard to predict. Think about the giraffe where we'd need to catch the hight of the neck on one side. 

idea: represent the object by its extreme points 

benefit: extreme points are still on the object 


 - How does ExtremeNet work?
   - predict extreme points of objects 
   - predict the center location of objects and according center for peak combination
   - accept if the center has high probability of being a center of a combination

      <img src="./figures/extremenet_giraffe.png" width=500>


## 6. Detection Evaluation


Precision $= \frac{TP}{TP + FP}$: out of all __detected objects__, how many were correct; we can have a high precision when just predicting a few objects but predicting correct 


Recall $= \frac{TP}{TP + FN}$: out of all __correct objects__, how many did the alg. get; i.e. coverage of ground truth


 - How to calculate the mAP?
   1. $\forall I_i \forall c_i$ rank predicted boxes by confidence score; assign to ground truth if $\text{IoU} > .5$
   2. each ground truth can only be matched once! 
   3. $\forall c_i$ compute average precision (AP)
   4. compute mean over all AP


 - Does the mAP change for low-score FPs when all TPs are covered by high-scores - why?
   - no, as the recall does not change as all FNs are covered already
   - FNs are covered, as all _Ts_ are above, due to high-scores
   - when recall does not change anymore, then the area under the curve does not change 
   

 - What is the problem with NMS on highly overlapping objects?
   - selecting the threshold too high, i.e. keeping boxes as different, can easily introduce false positive 
   - selecting the threshold too low, i.e. removing boxes assuming the same object is covered, introduces false negatives 
   - compare slides 66


## 7. Human Pose Estimation

Goal: same as box estimation now on joints 

Gets hard due to occlusions, extreme poses and viewpoint changes 


 - How does heatmap prediction work?
   - for each joint a full image is predicted with a heatmap at the joint's location
   - similar to ExtremeNet; instead of extreme points we go for joints 
   - ground truth is generated by placing a Gaussian around truth to have smooth gradient 
   - idea: it's easier to predict confidence per location than prediction on image 


 - How can the human anatomy be used?
   - introduce connection between joints, i.e. a body model, joint limits (s.t. elbow cannot bend backwards)
   - graphical models: cluster all proposed joints into joint types and ID, then predict by solving graphical/body model deciding to which instance the parts belong 
   - could also do object detection and then solving body model for each detected person


 - How are the convolutional filters in the Decoder learned, e.g. UNet?
   - backprop
   - goal: refine the upsampling yielding a high resolution image 
   - keeping spatial information might help (UNet adding feature maps to upsampled data); helps to get rid of artifacts in bi-linear interpolation when splashing
   - using skip connections, as UNet, spatial and semantic information is retained which might be lost in lower dim


 - How are hourglass networks used?
   - make a prediction
   - refine prediction after another high-to-low-res and low-to-high-res block -> __intermediate supervision__
   - hourglass predictions can be integrated via. $1 \times 1$ convolutions 
   - check slide 93 and following


 - Why do several steps of down- and up-sampling help in the Hourglass net?
   - helps refining the predictions 
   

 - How does the Hourglass algorithm predict joints?
   - same output as input image
   - but heatmap with high values indicating joints 
   

 - Where and why does the Hourglass architecture compute its loss?
   - at each intermediate output
   - helps to get a stronger feedback, i.e. training signal
   - intermediate supervision


## X. 

AdaBoost
 - Boosting algorithm which uses weak-learners at each stage but also bagging, i.e. re-weighted samples to train a new learner based on the prev. misclassification of that sample


Hard negative mining
 - Use the edge cases where the model fails.
 - Important to focus on the challenging samples


Why using gradients with HOG
 - as gradients are a strong que for boundaries and can be used to describe how boundaries of an object look; on average 


What is mAP as metric?
 - mean average precision 
 - average precision is computed by averaging over all precision results when changing the threshold between precision and recall, i.e. building the ROC 
 - mean average precision is then computed as the mean over all classes, i.e. mAP is mean over all classes where for each class the average precision is computed for each threshold 


Accounting for multilabels
 - multilabel is e.g. Woman and Person, i.e. one object having multiple correct labels 
 - using only Softmax activation (e.g. in YOLO) does not allow such data
 - Sigmoid on the other hand does allow high probability for different classes 
