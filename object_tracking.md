# Computer Vision III - Object Tracking 


General task in object tracking: 
 - identify parts in video sequence that depict the same object 
 - often, detectors are used to get a starting set of objects
 - used to reason about dynamic world, e.g. trajectory prediction (autonomous driving: person moves infront of car) 


Difficulties:
 - Occlusions -> disappearing and reappearing of objects 
 - viewpoint/pose/blur/illumination variations
 - background clutter 

 - Why do we need tracking?
   - detection can fail due to occlusion or other difficulties
   - to reason about dynamics in the world, e.g. trajectory prediction

Tools and metrics:
 - similarity between detected instances
 - correlation
 - correspondence 
 - matching
 - data association


 - What are our two major approaches, i.e. what are we modelling?
   - modelling __appearance__, i.e. how the target looks; used e.g. in re-identification
   - modelling __motion__, i.e. make predictions of where target goes (often RNNs)

    <img src="./figures/tracking.png" width="500">


## 1. Single Target Tracking

 - What are the three categories of single target tracking (STT)?
   - STT 1: matching/correspondence problem, e.g. GOTURN; no appearance model
   - STT 2: appearance learning problem, e.g. MDNet; quick online finetuning of the network
   - STT 3: as temporal prediction problem, e.g. ROLO having appearance model (CNN) and motion (LSTM)


#### Single Target Tracking 1

 - How does GOTURN work?
   - slow motion assumption, i.e. object cannot be far from the last position
   - basic structure is a siamese network - having the exact same weights -, through which the prev. frame and the current frame are passed
   - based on the prev. location, crop the frame to where the target is assumed
   - pass both target crops through the siamese net and refine the bounding box
   - Siamese CNN + concat. + FC

        <img src="./figures/goturn_structure.png" width=500>


 - What are the __pros__ and __cons__ of GOTURN?
   - no online training required as based on prev. frame
   - tracking based on comparison (similar to template matching) -> no refinement on new tasks 
   - very fast
   - BUT: motion assumption is a hard limit - not able to recover when objected moved too fast, e.g. out of the window


 - What is an improvement of the GOTURN?
   - tracking object back and forth in time, i.e. t+n and t-n
   - both tracks need to coincide; deviations are unsupervised loss


 - What does online appearance model training mean?
   - adapting the appearance model at test time
   - tweaking the model s.t. it fits the current data, i.e. learing the appearance of the object that is tracked 


#### Single Target Tracking 2

 - How does MDNet work?
   - shared layers for feature extraction, followed by domain specific layers per class
   - backpropagation per class updates the domain-specific layers as well as the __shared__ layers
   - during test time, appearance is learned by updating layers to a certain depth; but always the domain-specific ones (fine-tuning)

        <img src="./figures/mdnet_backprop.png" width=500>

 - How does online learning work for appearance modelling?
   - per frame, draw target candidates 
   - decide on the optimale box 
   - generate training samples around that optimal box which are used for fine-tuning
   - stop fine-tuning when certain accuracy is achieved 


 - What are the __pros__ and __cons__ of MDNet?
   - does not state assumptions; object can move anywhere in the scene
   - fine-tuning steps are comparatively cheap
   - BUT: is not as fast as GOTURN


#### Single Target Tracking 3

 - How does ROLO work?
   - using CNN for appearance modeling and LSTM for motion modeling
   - YOLO provides heatmap of obj's position and 4096 encoding to LSTM
   - LSTM predicts next motion (LSTM can roughly estimate motion)

        <img src="./figures/rolo_structure.png" width=500>


## 2. Multiple Object Tracking 

 - What are the challenges in MOT compared to SOT?
   - multiple objects of the same type and similar appearance 
   - heavy occlusions by each other
   - learning an appearance model is now very difficult 


 - What are problems with tracking-by-detection in MOT?
   - false positive, i.e. wrong trajectory, might influence later predictions
   - false negative, i.e. stopping trajectories too early


 - Properties of online and offline tracking?
   - online is faster, i.e. for real-time applications but very hard to recover from errors, might even accumulate 
   - online uses two frames at a time: t and t+1
   - offline can process way more frames, travel in time t-n and t+n, able to recover occlusions and good for video analysis
   - offline uses batch of frames (possibility to recover from occlusions) and thus not able to work real-time, i.e. online


 - How does online tracking match?
   - take detections and predictions (t+1)
   - compute distances between all; small distance due to motion assumption 
   - solve bipartite matching, e.g. Hungarian


 - How to overcome when there are less predictions or less detections?
   - try to prevent false matchings
   - introduce a threshold cost, the lower the better
   - if an instance has no counterpart with lower distance, match with the dummy
   - lower thresholds foster true positive matching 


 - What are the steps of online tracking?
   1. init track, e.g. detector
   2. predict next position; motion model, i.e. temporal complexity
   3. match predictions with detections; appearance model, i.e. feature complexity
      - improving appearance models, e.g. re-identification
      - couple matching with learning (currently decoupled)


 - How does tracktor work?
   - use just a detector 
   - use detection from frame t-1 as proposals for frame t
   - do not use any other region proposals as in detection
   - bounding box refinement on frame t using proposals, i.e. map into feature space following fully connected for refinement


 - How can one with a *-RCNN implement tracktor?
   - use the box regressor as such are very well trained on refining bounding boxes
   - detection from $t-1$ is fed to bounding box regressor which refines the box
   - with the refined box, one knows where the box moved from $t-1$ to $t$


 - What are the __pros__ and __cons__ of tracktor?
   - usage of very well trained regressor, e.g. Faster R-CNN, for well positioned bounding boxes
   - trainable on still image, easier annotation
   - online tracking as we could skip region proposal step; proposal is the detection at $t-1$
   - BUT: no notion of identity, problematic in crowded scenes, online = killing track when disappearing, as proposal is shifted only little, large camera motions and displacements (e.g. low fps) pose problems


 - How can tracktor be enhanced?
   - re-identification for lost tracks and pose changes
   - motion model for fast moving objects or scene changes, e.g. how the camera is moving and where to expect the obj.


## 3. Re-Identification

loosing an object due to occlusion or missed detection means terminating a trajectory. While, in the case of occlusion, the object will re-appear and thus be tracked again, the object needs to be identified as the same that was lost before. Further, this can help to bridge frames in which the object could not be detected. 

Idea: tracking as retrieval problem -> bunch of samples in which one wants to find a probe; the possible matches are then retrieved and ranked


#### Similarity Learning

Goal: learn a function, measuring similarity between objects; using DL, we want to learn a distance function over objects 


 - What is a problem of simple similarity learning (when phrased as classification)?
   - Having to retrain the system for every new object in the system 
   - Solution: learn a similarity function instead of a classification; then decided based on threshold: $d(X,Y) \leq \tau \to \text{same}$
   - learn good value $\tau$


 - How can similarity learning be integrated into retrieval approach?
   - compute distance from probe to all samples
   - use k-NN to get closest ones 


 - How is the similarity network constructed?
   - use siamese network; process both input with the exact same config
   - shared weights 
   - usually CNN + fully connected 

      <img src="./figures/siamese_similarity.png" width=500>


 - What is a good loss function to use for similarity learning?
   - Hinge loss
   - don't spend compute on negative samples that are already far away 
   - but pull same samples even closer together 
   - $\mathcal{L}(X,Y) = y\|f(X) - f(Y)\|^2 + (1-y)\max(0, m^2 - \|f(X) - f(Y)\|^2)$
   - where $m$ is the margin


 - How can the combination of distance and hinge loss be improved?
   - use triplet loss
   - enables to learn a ranking 
   - not caring about the actual distance but positives $P$ must be closer together than a positive to negatives $N$
   - $\| f(X) - f(P) \|^2 < \| f(X) - f(N) \|^2$
   - include a margin $m$ to foster greater distances to negatives: $\| f(X) - f(P) \|^2 < \| f(X) - f(N) \|^2 + m < 0$
   - margin $\sim$ "separate positives and negatives by at least $m$"
   - again, think about only learning as much as needed, i.e.
   - $\mathcal{L}(X,P,N) = \max(0, \| f(X) - f(P) \|^2 - \| f(X) - f(N) \|^2 + m)$

      <img src="./figures/triplet_loss.png" width=500>


 - What are problems with triplet loss?
   - information thrown away as not using any other samples, just one positive and one negative
   - requires tricks to train, e.g. hard-negative mining ($\sim$Adaboost), intelligent sampling
   - uses less relations than possible: $\mathcal{O}(\frac{2n}{3}) \text{ vs. } \mathcal{O}(n^2)$


 - How does Group Loss work?
   - per minibatch
   - compute embedding of images in different classes using the _same_ CNN for all -> compute similarity matrix
   - compute softmax classification for all to get a class prior, i.e. idea of distribution
   - refine prediction iteratively
     - using batch similarities and priors 
     - iterate re-computing support that a sample is from a class
     - propagate that information
     - math on slide 83
   - cross-entropy loss and backprop., i.e. classification loss
   - Group Loss has no parameters but propagates gradients over network

      <img src="./figures/grouploss_vs_triplet.png" width=500>

      <img src="./figures/grouploss_refinement.png" width=500>


- Why is Group Loss more powerful than triplet loss?
  - uses __all relations__ between samples in the minibatch


 - What are problems with frame-by-frame tracking?
   - cannot recover from errors, e.g. missing detections
   - decisions are always __local__, i.e. based on the current frame 
   - SOLUTION: minimize cost for a solution over all fames and trajectories - we are offline now


## 4. Message Passing Networks

MPNs are a graph using messages, along all edges between the nodes, to update a node with its neighbors. It is important, that the message aggregation has to happen with an __order invariant method__ as we don't pose any order on the nodes. A layer of the network corresponds to the hops an information can travel from one node to another, i.e. for 3 layers, an information can travel 3 nodes away. Further, the information is given in an aggregated form of node-embedding $h_v^{(i)}$ and the edge's embedding $h_{(u,v)}^{(i)}$, called a message $m_v^{(i)}$. 

<img src="./figures/mpn_basic.png" width=500>

For an update, first the message to a node is computed by collecting information from all its neighbors:

<img src="./figures/mpn_computing_message.png" width=500>

and secondly updating the node embedding:

<img src="./figures/mpn_computing_embedding.png" width=500>

In the steps above, the update functions $U$ and $M$ for the embeddings can be any order-invariant information, e.g. a neural network or mean, working on the embedding and being shared for all nodes in the current layer. The update to a node's embedding is then passed through an activation.

A full example, where the message includes the current node as well (self-loop) is shown below:

<img src="./figures/mpn_example_with_self_loop.png" width=500>


 - Why do we divide with the size of the neighborhood when computing the message update?
   - to not invite a bias due to neighborhood size 

Note, until now only the nodes $h_v^{(i)}$ were updated and not the edges $h_{(u,v)}^{(i)}$. Updating the network is done in two steps, the edge-node update as well as the node-edge update:

<img src="./figures/mpn_updating_edges.png" width=500>

To compute the node-to-edge update, a function acting on the concatenated embeddings of the adjoint nodes and the embedding of the current edge computes a new embedding.

<img src="./figures/mpn_node_to_edge_update.png" width=500>

To finish, the node updates, as seen above, where $\phi$ is an invariant function:

<img src="./figures/mpn_edge_to_node_update.png" width=500>


 - How does tracking-by-detection in the case of MOT work?
   - (1) generate detection, e.g. Faster R-CNN (2) data association using GNN
   - nodes = detections
   - edges = relationship between two detections
   - graph network has to capture higher-level features between detections via message passing
   - when embeddings have converged after message passing, binary classification per edge is applied
   - active edge = objects belong to one another 
   - forming trajectories by grouping nodes into disconnected components


 - Why are edge updates important?
   - edges contain information about pairs of incident nodes, i.e. detections 
   - in the aggregation step, the nodes then receive information about their neighbors 
   - this information is used by the GNN to capture higher-level information about detections 


 - The detections need to be processed in two ways, which and how?
   - appearance and motion/geometrically
   - nodes = appearance
   - edges = motion


 - Name the pipeline of the MPN for MOT
    1. Graph construction = detection on all frames in the sequence 
    2. Feature encoding = CNN on all bounding boxes and MLP on timestamp (distance in time), position and box coordinates 
    3. Message passing = updating edges and nodes until convergence 
    4. Training = binary classification of converged edges into active and passive edges; training uses CE

        <img src="./figures/mpn_mot_pipeline.png" width=500>


 - How can the final step, edge classification (training), be done?
   - goal: learn to classify edge embeddings
   - put a Net for a binary classification on each edge embedding
   - i.e. MLP predicts if edge is active or not


 - How is the model used during test time?
   - take model's edges and round them, i.e. get $h_{(u,v)} \in \{0,1\}$


 - What is time-aware message passing?
   - breaks aggregation step into two parts to encode the __temporal structure of the graph__
   - do aggregation of frames in the past; do aggregation of frames in the future
   - keep both aggregations for t-1 and t+1 separate, i.e. t is a cut-off for aggregation 
   
      <img src="./figures/mpn_time_awarenes.png" width=500>


 - How is time-aware message passing computed?
   - given messages at time $m_t, t \in \{1,2,3,4\}$ and current time $t=2$ and weight matrix $\mathbf{M}$
   - then compute the messages separately per time interval, i.e. prior and past $t$, e.g. $\operatorname{mean}$
   - concatenate the messages and multiply with weight matrix
   - e.g. if $\hat{m}_{t \leq 2} = (0.5, 0.5)^{\prime}$ and $\hat{m}_{t \geq 2} = (0.5, 0.5)^{\prime}$ with $\mathbf{M} = [(1,1,1,1), (1,1,1,1)]$; yields after concat. of $\hat{m}_i$: $\mathbf{M} \cdot (.5,.5,.5,.5)^{\prime} = (2,2)^{\prime}$


#### MOT as linear problem 

 - How is the cost function for MOT with network flow constructed?
   - entrance and exit cost: only start a track, if the model is really sure about it, not just when re-appearing
   - transition cost: approx. distance between detections; far lower than entrance/exit
   - only connected transitions are relevant for cost function 
   - need for __a__ negative cost; otherwise trivial solution $=0$ -> detection cost is negative
   - detection cost: $C_{\text{det}} = \log \frac{\beta_i}{1 - \beta_i}$ reflects how "confident" the model is about a new detection; $\beta$ reflects probability of false alarm -> if confident about detection, then $C_{\text{det}} \to -\infty$


 - Why are all detections connected with start and sink node?
   - to allow to start and end a trajectory at any detection
   - regulate where to start via costs 


 - Why is this formulation not used?
   - requires linear optimization -> fast
   - requires integer edges -> NP hard
   - can be relaxed -> again faster
   - a lot of manual tweaking etc. of the system 
   - __MPN is end-to-end learnable__ as all operations are differentiable


## 5. Metrics 

 - What is MOTA?
   - perform matching between predictions and ground truth
   - multi-object tracking accuracy: $\text{MOTA} = 1 - \frac{\sum_t \text{FN}_t + \text{FP}_t + \text{IDSsw}_t}{\sum_t \text{GT}_t}$
   - interpret: $1-$ ratio of wrong to correct, i.e. ratio of correctness 


 - What is IDsw?
   - ID switch, i.e. same object changing trajectory
   - e.g. trajectory is first assigned to ID1 then to ID2
   - dashed line is ground truth
   - a) ID switch b) ID switch and fragmentation, i.e. both are counted as ID switch c) ground truth is assigned to track with less ID switches as long as the overlap stays within a boundary

      <img src="./figures/mot_idswitch.png" width=500>
