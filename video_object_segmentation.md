# Computer Vision III - Video Object Segmentation

__Goal:__ accurate and consistent pixel masks for objects in a video sequence

__Challenges:__ 
  - viewpoint changes, e.g. via camera movement
  - appearance changes via object deformation and movement
  - occlusion
  - scale changes, e.g. object moving into focus
  - illumination
  - shape
  - ...
  
---

 - What higher level challenges do the above induce?
   - occlusion: motion prediction hard
   - viewpoint, appearance, scale changes: appearance learning hard


 - What is semi-supervised VOS and what is unsupervised VOS?
   - semi-supervised: provided with segmentation of first frame -> know which objects to track
   - unsupervised: have to find objects and masks alone


<img src="./figures/vos_methods.png" width=500>


## 1. Optical Flow

Idea to get the __perceived__ 2D motion using the difference in two consecutive images, i.e. the displacements per pixel.


 - What else is an influence to the Optical Flow?
   - camera movement
   - should be static 


 - What's a nice byproduct of Optical Flow with static camera?
   - coarse segmentation of moving objects
   - can be used as region proposals 


 - What is Flow Net?
   - (fully) CNN computing optical flow on two images
   - two types:
     - concatenate images, pipe through CNN, refinement CNN, predictions
     - compute differences via siamese network, correlation, CNN \& refinement, predictions


 - How does FlowNet combine the outputs of the Siamese net?
   - Correlation layer, i.e. correlation of features = how similar are features in feat. map 1 and map 2?
   - no parameters
   - (1) flatten feature maps into $W \cdot H \times C$ per output
   - (2) compute inner product for all pairs of vectors, i.e. rows

        <img src="./figures/flownet_correlation_layer.png" width=500>


 - What are benefits of correlation layer?
   - very fast as no parameters


 - What is the output size of the correlation layer for $W \cdot H \times C$ flattened inputs?
   - got $W \cdot H$ vectors in each map $\to$ $W \cdot H \times W \cdot H$


## 2. One-Shot VOS

Focussed on lerning an appearance of the object to track. 

1. pre- train to learn objectness
2. on first frame learn appearance of object 


 - How does OSVOS work?
   - use pre-trained network for edges and basic features 
   - train for video (object) segmentation -> just segment all objects in the foreground
   - fine-tune model on first frame on test seq. -> to know which obj. to segment

      <img src="./figures/osvos_pipeline.png" width=500>


 - How does OSVOS learn the appearance?
   - via fine-tuning on first frame of test seq., i.e. overfitting
   - use augmentation etc. to improve generalisation 


 - How does OSVOS process the frames at test time?
   - one by one 
   - no notion of time/sequence 
   - i.e. frame-based segmentation


 - What are __pros__ and __cons__ of OSVOS?
   - recovers well from occlusions unlike mask propagation or optical flow
   - but: only appearance of first frame, changes in the seq.
   - also no notion of a seq. -> temp. inconsistent
   - pure-appearance based $\to$ __fails__ if fore- or background change too much


 - How is the drawback of only overfitting to the first frame noticed?
   - model has difficulties to differentiate between similar objects when entering the scene
   - overfitting on 2 or more of the first frames helps 

        <img src="./figures/osvos_improved_via_more_frames.png" width=500>


In the image below the model was fine-tuned on the dancer in the middle but did not know about the guy in the background appearing in frames later than the first ones. Thus, the guy is not detected as background -> false positive.

<img src="./figures/osvos_fail_background.png" width=500>


## 3. OSVOS-S

The drawback of OSVOS is that is purely appearance based and also only learned the appearance through the first couple of frames. As instance segmentation methods already have a good notion of object shapes, we can incorporate those to yield an appearance-prior.


 - How is OSVOS-S structured?
   - two branches (1) OSVOS (2) segmentation as appearance prior
   - (1) OSVOS overfits on first frame, tries to predict foreground
   - (2) Segmentation of input into possible objects, get first pred. of OSVOS as hint, refine proposed objects
   - together, refine segmentation
   - OSVOS knows what to segment, segmentation knows how to

        <img src="./figures/osvos_s_pipeline.png" width=500>


 - Why does segmentation improve OSVOS so much?
   - OSVOS segmentation gets worse over time as object differs from initial frame
   - but the OSVOS seg. still has some overlap with the initial obj.
   - this uses segmentation to segment the correct object 


 - What is the drifting problem?
   - object or camera changes
   - appearance seems to have changed greatly 
   - result: no overlap anymore with tracked object
   - idea: also adapt appearance model over time to keep track of the changes 


 - How can OSVOS(-S) be applied online?
   - fine-tune on every new frame, i.e. update appearance model
   - BUT: extremely slow


## 4. Mask Track

 - What is Mask Refinement?
   - assume masks only change little from frame to frame
   - start with a guess and use _mask refinement_ model to refine mask predictions
   - can take advantage of crop-and-zoom to segmentation on higher dimension


 - What is Mask Track and how does it work?
   - get frame and mask at $t-1$
   - pipe new input and old through CNN to refine mask from $t-1 \to t$
   - trained on static images
   - either needs slow motion or high framerate 

        <img src="./figures/mask_track_pipeline.png" width=500>


 - How can Mask Track be improved?
   - similar to Tracktor
   - when training the refinement, augment input 
   - i.e. via augmentations we get a better idea how the object might have moved


## 5. Proposal Based Approaches

Idea: instead of taking the whole image as input and then refining on top, take only proposals and link them. Similar to tracking-by-detection.

Similar as taking each frame, finding proposals via e.g. Mask-RCNN, and linking such.


 - What approaches does PReMVOS use?
   - Proposal generation, class-agnostic via Mask-RCNN; e.g. boy and bike
   - Refinement, via fully conv. to refine boxes given a proposal bounding box; e.g. refine boy and bike
   - Merging; e.g. merge boy and bike into one mask

        <img src="./figures/premvos_steps.png" width=500>


## 6. Spatio-Temporal Approaches

_While more models were discussed, I only note down R-VOS._

Idea: take temporal information into account as well as only spatial (appearance) information was used yet.


 - What is spatial recurrence?
   - per frame use a CNN for feature extraction and a LSTM to detect a sequence of objects 
   - seeing instances as seq. allows to extract as many instances as there are in one frame, i.e. no hyper param.


 - What is spatio-temporal recurrence?
   - use feature extraction and sequence evaluation in both directions
   - i.e. include temporal information between frames as well
   - encoded via CNN (vis. features) and LSTM (sequence/temporal domain)


<img src="./figures/spatio_temp_approach.png" width=500>


## 7. Evaluation Metrics

 - Region Similarity, i.e. IoU of GT and prediction
 - Contour Accuracy: F-measure $= \frac{2 \cdot \text{precision} \cdot \text{recall}}{\text{precision} + \text{recall}}$
 - Temporal Stability: measures stability of boundaries over time; bad mask = jittery, good mask = smooth; _was dropped_ as unstable during occlusions


## X.

 - What is the difference between VOS and MOT?
   - VOS: short videos, first frame mask given, few objects to track, usually from different categories
   - MOT: long sequences, no first-frame-annotation, _many_ objects to track, mostly of the same kind
   - and: segmentation vs. tracking
