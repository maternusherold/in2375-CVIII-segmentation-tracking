# Computer Vision III - Transformers \& DETR


Attention is starting a deep learning revolution, i.e. is being used in many different domains of DL - NLP, CV, ... and CNNs are used together with transformers. Also, _attention is all you need_ got over 20k citations in less than 3.5 years.


 ## 1. Transformers

  - What are the key challenges with RNNs?
    - vanishing gradients 
    - long-term memory dependency solved via LSTM but that limits long-term memory; works well for sentences but not complete texts
    - needs similar structure but hard when keywords come at different positions 


 - What is the key difference between attention and conv./mlp?
   - model can learn where to draw information from
   - for mlp and cnn, the connection are hard coded, i.e. connections and kernel size 
   - for attention, the connections can vary

        <img src="./figures/conv_vs_attention.png" width=500>


 - How does Multi-Head Attention work?
   - basically, given query Q, get similar key K and the value V to that key -> Q, K, V are matrices
   - $\text{attention}(Q,K,V) = \operatorname{softmax} \frac{QK^T}{\sqrt{d_k}} V$
   - $\sqrt{d_k}$ needed to conquer large values, otherwise vanishing grad. in softmax
   - keys span a space, most $k_i \in K$ are orthogonal as embedded in very high dim. 
   - $QK^T$ has large value at $k_i$ close to Q
   - thus: $\operatorname{softmax} \frac{QK^T}{\sqrt{d_k}}$ induces distribution over values $v_i \in V$ and get a peak of the dist. at the value according to the key
   - just indexing $V$ would drop all other information of the rest of $k_j, j \neq i$ thus multiplying with $V$


 - What is the difference between Masked Multi-Head Attention and just Multi-Head Att.?
   - "masked" ensures predictions for position $i$ can only depend on outputs of positions less than $i$


 - Why do we need positional encoding in Transformers?
   - essentially a encoder/decoder architecture but no notion of order 
   - need someway to encode order 
   - positional encoding = information about element's position in the input 


 - How does the complexity compare between Self-Attention and Convolutions?
   - attention: $\mathcal{O}(n^2 \cdot d)$
   - convolution: $\mathcal{O}(k \cdot n \cdot d^2)$
   - n: sequence length; d: dimension; k: kernel size 
   - attention more performant iff $n \leq d$
   - e.g. sentences are usually shorter than dim. of representation 


## 2. DETR

Applies the idea of attention to computer vision and achieves (near) SOTA results in detection and can be applied to panoptic segmentation with minor changes. Further, it gets rid of NMS and thus the problems when picking the threshold. 


 - What is DETR - short?
   - end-to-end object detection with Transformers 
   - uses CNN for feature learning
   - uses Transformer to make predictions 


 - How is DETR structured?
   - feature extraction using CNN
   - prediction using Transformer
   - runs in parallel
   - number of predictions is hyperparameter 
   - bipartite matching (e.g. hungarian) with ground truth for training 
   - __no need for Non-max suppression__


 - Why does DETR use a combination of $\mathcal{L}_1$ and IoU loss?
   - as DETR makes box predictions directly, there's an issue with relative scaling w.r.t. the loss
   - thus, $\mathcal{L}_1$ will have different scales for small and large boxes, even if they're relatively equal in performance 


 - Describe the DETR's pipeline:
    1. learn 2D feat. representation with CNN + flatten features
    2. supplement features with positional encoding -> pass to transformer
    3. transformer encoder -> small number of positional embeddings 
    4. transformer decoder; takes embeddings and object queries (hyperparam.) and also attends to the encoders output -> output embedding
    5. FFN predicting either detection or no class based on output embeddings 

        <img src="./figures/detr_overview.png" width=500>


The DETR is able to separate individual instances and thus qualifies for instance/panoptic segmentation. Further DETR usually attends to object's extremities (compare ExtremeNet), s.a. heads or legs.


<img src="./figures/detr_attention_instances.png" width=500>
<img src="./figures/detr_attention_extremities.jpg" width=500>


 - How is DETR adapted for panoptic segmentation?
   - generate binary mask for each detected object in parallel to detection
   - merge masks using pixel-wise $\operatorname{argmax}$
   - gives us: prediction that pixel belongs to cow and also to which cow
   - outperforms specific panoptic seg. models, e.g. UPSnet


 - What are drawbacks of DETR?
   - training very long; 3 days on 16 V100
