# Computer Vision III - Trajectory Prediction

Predicting trajectories, e.g. of pedestrians, is a key component for navigation in autonomous cars or robotics. While the motion of a human is influenced by a variety of factors, s.a. destination, personal preferences (fast, slow, drunk), human-space interactions (humans need doors and cannot pass though walls) and human-human interactions, those factors are usually intrinsic.


Further predicting the trajectory of an object improves motion modelling and thus improves tracking.


#### Social Force Model

 - What and how deos the social force model describe trajectories?
   - mathematical model of dynamics
   - pedestrians act on force field like particles -> PDE describe movement 
   - applies different forces onto an agent; via other humans, obstacles and drawn towards the destination


 - How is the force between pedestrians described?
   - gradient of repulsive potential $\mathbf{V}_{\alpha\beta} = \mathbf{V}^0 \exp - \frac{\|\overset{\to}{r}_{\alpha\beta} \|}{\sigma}$ 
   - $\mathbf{V}, \sigma$ shape parameters, i.e. effect strength of interactions
   - at $\sigma \approx 2.1$ an effect is visible


 - What are the drawbacks of the Social Force Model?
   - requires many parameters for each agent, agent-agent and agent-environment pairs
   - exploding DoF and functions are _handcrafted_ but would like to learn them 


 <p style="color: red; weight: bold">mainly skipped the social force model ...</p>


## 1. Learning Trajectory Prediction 


 - Why are fully connected networks not suitable for traj. prediction?
   - do not account for sequential and temporal behavior
   - i.e. would favour RNNs


 - Why does a single LSTM not work well?
   - because we have to use the same network, i.e. the weights etc. to encode an input trajectory and to predict a new trajectory of the same size
   - that's a task for a encoder-decoder architecture
   - thus: LSTM to encode and LSTM to decode trajectory


        <img src="./figures/traj_pred_encoder_decoder.png" width=500>


 - What are problems of just using an LSTM encoder-decoder structure?
   - there are no interactions modelled yet
   - i.e. model fails to predict those
   - no stochasticity, i.e. only deterministic movements 
   - but does a good job on samples w/o interactions 


#### Social LSTM

 - What was introduced by the social LSTM?
   - social pooling between neighbors, i.e. taking interactions/other actors into account as well
   - hidden states are shared with neighbors; encoder-decoder structure is maintained 
   - while social pooling can resolve interactions, the _motion is still linear_


 - How does social pooling work?
   - idea: share hidden states with neighbors -> separate area into grid reflecting distance
   - hidden states of close objects are added 
   - pooled states are then passed to the _encoder_ for the next step
   - in the img. below: (1) yellow, (2) blue and (3) orange are close to black; (1) and (2) are in same cell, i.e. add their states; pass (1+2) and (3) to encoder of black for the next step


        <img src="./figures/traj_pred_social_pooling.png" width=500>


 - What's the problem with deterministic models?
   - provide one-to-one mapping, minimizing the loss
   - e.g. crossing, min. loss in expectation would be straight but not realistic
   - we want: (multimodal) distribution over trajectories 


#### Generative Models for Trajectory Prediction

 - Why are generative models introduced for traj. prediction?
   - incorporate stochastikcity to get a distribution over trajectories 


 - What are models introduced in the lecture?
   - Variational Autoencoder
   - GANs


 - Why do we need regularization in the variational autoencoder?
   - to yield a meaningful output for some input
   - otherwise some points in the latent space are decoded meaningless 
   - reg. via KL-divergence


 - What is the idea of the best-of-many architecture?
   - encoder-decoder architecture
   - encode both label and observation
   - condition latent distribution on both x and y, s.t. $q_{\theta}(z|x,y)$
   - during training sample multiple trajectories and backprop. the best one
   - during test time, x is still observation and we sample z from latent dist. 
   - so we now get a distribution over all possibilities as not just the overall most likely trajectory is sampled 

        <img src="./figures/traj_pred_vae.png" width=500>


 - How is the GAN for traj. pred. structures?
   - uses encoder-decoder structure 
   - still inputs true traj. to get a representation of feasible traj.
   - adds encoding of true traj. with noise for the decoder -> augmented traj. 
   - augmented and true traj. are fed to decoder -> true/false

        <img src="./figures/traj_pred_gan.png" width=500>


 - How is Social GAN (SGAN) structured?
   - contrib.: GAN + social interactions 
   - uses again LSTMs as encoder and decoder 
   - again has (social?) pooling module 
   - generator: encoder (LSTMs) - pooling - decoder (LSTMs) -> augmented
   - discriminator: augmented -> encoder (LSTMs) -> true/false

        <img src="./figures/traj_pred_social_gan.png" width=500>


 - How does the SGAN pooling module work?
   - TODO...


#### Bicycle GAN

<p style="color: red; weight: bold">TODO ...</p>


 - What is Social BiGAT's contribution?
   - combines bicycle GAN with graph attention
   - __modelling social interactions with graph attention__


## x. Recap

 - What is pedestrian trajectory prediction used for?
   - esp. autonomous driving and robotics
   - general, for all applications with interactions 
   - here: __used to improve tracking__


 - What is the task of pedestrian trajectory prediction?
   - model the upcoming movement of an individual which depends on intrinsic factors 


 - What makes pedestrian trajectory prediction challenging?
   - intrinsic factors
   - stochasticity needed


 - What kind of models are used for trajectory prediction?
   - deterministic: LSTMs
   - stochastic: GANs, VAEs


 - How are social interactions modelled?
   - attention, pooling, or graph networks 
   - esp. social pooling, social max-pooling, soft-attention, graph attention
   - ...

 - How are agent-scene interactions modelled?
   - attention, pooling, or graph networks
   - esp. soft-attention
   - ...
